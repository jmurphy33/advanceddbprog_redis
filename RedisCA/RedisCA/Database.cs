﻿using StackExchange.Redis;


namespace RedisCA
{
    public class Database
    {
        IDatabase db;

        public Database()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost:6379");
            db = redis.GetDatabase();
        }

        public void Create(string key, string value)
        {
            db.StringSet(key, value);
        }

        public void AddtoSet(string key, string value)
        {
            db.SetAdd(key, value);
        }

        public void AddtoListFromLeft(string key, string value)
        {
            db.ListLeftPush(key, value);
        }

        public void AddtoListFromRight(string key, string value)
        {
            db.ListRightPush(key, value);
        }

        public string Read(string key)
        {
            return db.StringGet(key);
        }

        public string ReadSet(string key)
        {
            string setValues = "";

            foreach (var item in db.SetMembers(key))
            {
                setValues = setValues + "\n" + item.ToString();
            }

            return setValues;
        }

        public string ReadList(string key)
        {
            string listValues = "";

            foreach (var item in db.ListRange(key))
            {
                listValues = listValues + "\n" + item.ToString();
            }

            return listValues;
        }

        public string ReadBlockingList(string key)
        {
            if (ListCount(key) != 0)
            {
                return db.ListLeftPop(key);
            }
            else
                return "No new messages in the list...";
        }

        public void Update(string key, string value)
        {
            db.StringSet(key, value);
        }

        public void Delete(string key)
        {
            db.KeyDelete(key);
        }

        public void RemoveFromSet(string key, string value)
        {
            db.SetRemove(key, value);
        }

        public void RemoveFromListFromRight(string key)
        {
            db.ListRightPop(key);
        }

        public void RemoveFromListFromLeft(string key)
        {
            db.ListLeftPop(key);
        }

        public void RemoveFromList(string key, string value)
        {
            db.ListRemove(key, value);

        }

        public long ListCount(string key)
        {
            return db.ListLength(key);

        }

        public long SetCount(string key)
        {
            return db.SetLength(key);

        }

    }
}

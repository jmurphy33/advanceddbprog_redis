﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using System.Threading;

namespace RedisCA
{
    public class Program
    {
        static EventWaitHandle waitHandle = new AutoResetEvent(false);
        static Database db;
        static void Main(string[] args)
        {    
            try
            {
                db = new Database();
                RemoveFromList("EA:devs","Dice");

                //db.AddtoListFromLeft("ChatList", "Messages are now open to be received....");
                //Thread t = new Thread(() => StartCheck("ChatList"));
                //t.Start();

                //while (true)
                //{
                //    DateTime start = DateTime.Now;
                //    waitHandle.WaitOne(300000);
                //    TimeSpan timeItTook = DateTime.Now - start;

                //    if (db.ListCount("ChatList") > 0)
                //    {
                //        Console.WriteLine("Blocked for:\t" + timeItTook + "\nMessage:\t" + db.ReadBlockingList("ChatList"));
                //    }                            
                //}           

                Console.WriteLine("Worked");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public static void StartCheck(string key)
        {
            while (true)
            {
                if (db.ListCount(key) > 0)
                {
                    waitHandle.Set();
                }
            }
            
        }

        static void Create(string key, string val)
        {
            Console.WriteLine("creating: "+key+ " value:"+val);    
            db.Create(key, val);
        }

        static void AddToSet(string key, string val)
        {
            Console.WriteLine("Add/Update "+key + " set with value: "+val );      
            db.AddtoSet(key, val);
        }

        static void AddToListLeft(string key, string val)
        {
            Console.WriteLine("Adding to "+key +" list from left with: " + val);
            db.AddtoListFromLeft(key, val);
        }

        static void AddToListFromRight(string key, string val)
        {
            Console.WriteLine("Adding to " + key + " list from right with: " + val);
            db.AddtoListFromRight(key, val);
        }

        static void RemoveFromSet(string key, string val)
        {
            Console.WriteLine("Removing " + val + " from: " + key);
            db.RemoveFromSet(key, val);
        }

        static void RemoveFromListRight(string key)
        {
            Console.WriteLine("Removing right most value from: " + key);
            db.RemoveFromListFromRight(key);
        }

        static void RemoveFromListLeft(string key)
        {
            Console.WriteLine("Removing left most value from: " + key);
            db.RemoveFromListFromLeft(key);
        }

        static void RemoveFromList(string key, string val)
        {
            Console.WriteLine("Removing " + val + " from: " + key);
            db.RemoveFromList(key, val);
        }

        static void Read(string key)
        {
            Console.WriteLine("Reading: " + key);
            Console.WriteLine(db.Read(key));
        }

        static void ListCount(string key)
        {
            Console.WriteLine(key+ " List Count: "+ db.ListCount(key));
        }

        static void SetCount(string key)
        {
            Console.WriteLine(key+ " Count: " + db.SetCount(key));
        }

        static void ReadSet(string key)
        {
            Console.WriteLine(key+" contains:"+db.ReadSet(key));
        }

        static void ReadList(string key)
        {
            Console.WriteLine(key +"Contains:"+db.ReadList(key));
        }

        static void Update(string key,string val)
        {
            Console.WriteLine("Updating: " +key + " with value: "+val);
            db.Update(key, val);
        }

        static void Delete(string key)
        {
            Console.WriteLine("Deleting key: "+ key);
            db.Delete(key);
        }
    }
}
